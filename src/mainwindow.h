#include <QMainWindow>
#include <QString>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
  private:
    Ui::MainWindow *ui;
  
  public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
  
//  public slots:
};
