#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "StorageInfo.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    auto sMgr = StorageManager::instance();

    for( StorageDevice dev: sMgr->devices() ) {
        /* If it's not removable and not optical then it's probably a HDD */
        if ( not dev.isRemovable() and not dev.isOptical() ) {
            qDebug() << dev.label() << dev.size();

            for( StorageBlock block: dev.partitions() ) {
                qDebug() << "BLOCK: " << block.property( "Block", "HintSystem" ).toString();
                if ( not block.property( "Block", "" ).toBool() ) {
                    qDebug() << "    " << block.device() << "(C)";
                    qDebug() << "        " << block.label();
                    qDebug() << "        " << block.mountPoint();
                    qDebug() << "        " << block.property( "Partition", "Number" ).toUInt();
                    qDebug() << "        " << block.property( "Partition", "Type" ).toString();
                    qDebug() << "        " << block.totalSize();
                }
/*
                else if ( block.fileSystem() == "swap" )
                    qDebug() << "    " << block.device() << "(Swap)";
*/
                else {
                    qDebug() << "    " << block.device() << "NC";
                    qDebug() << "        " << block.label();
                    qDebug() << "        " << block.mountPoint();
                    qDebug() << "        " << block.property( "Partition", "Number" ).toUInt();
                    qDebug() << "        " << block.property( "Partition", "Type" ).toString();
                    qDebug() << "        " << block.totalSize();
                } 
            }
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
